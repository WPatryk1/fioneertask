import { ContactUsPage } from '@_src/pages/contactUs.page';
import { FinancialControlPage } from '@_src/pages/financialControl.page';
import { Locator, Page } from '@playwright/test';

export class TopMenu {
  constructor(private page: Page) {}

  //Locators for all bookmarks from top menu
  private allBookmarksLocator: Locator = this.page.locator('#masthead');
  readonly bankingBookmarkLocator: Locator = this.allBookmarksLocator.getByRole('link', { name: 'Banking' });
  readonly insuranceBookmarkLocator: Locator = this.allBookmarksLocator.getByRole('link', { name: 'Insurance' });
  readonly financeEsgBookmarkLocator: Locator = this.allBookmarksLocator.getByRole('link', { name: 'Finance & ESG' });
  readonly servicesBookmarkLocator: Locator = this.allBookmarksLocator.getByRole('link', { name: 'Services' });
  readonly partnersBookmarkLocator: Locator = this.allBookmarksLocator.getByRole('link', { name: 'Partners' });
  readonly companyBookmarkLocator: Locator = this.allBookmarksLocator.getByRole('link', { name: 'Company' });
  readonly resourcesBookmarkLocator: Locator = this.allBookmarksLocator.getByRole('link', { name: 'Resources' });
  readonly getInTouchButton: Locator = this.allBookmarksLocator.getByRole('link', { name: 'Get in touch' });

  //Finance & ESG bookmark menu locators
  readonly financialControlLinkLocator: Locator = this.page.getByRole('link', {
    name: 'Financial Control',
  });

  async clickFinancialControlLink(): Promise<FinancialControlPage> {
    await this.financeEsgBookmarkLocator.hover();
    await this.financialControlLinkLocator.click();
    return new FinancialControlPage(this.page);
  }

  async clickGetInTouchButton(): Promise<ContactUsPage> {
    await this.getInTouchButton.click();
    return new ContactUsPage(this.page);
  }
}
