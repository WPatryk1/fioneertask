import { BasePage } from './base.page';
import { Locator, Page } from '@playwright/test';

export class FinancialControlPage extends BasePage {
  constructor(page: Page) {
    super(page);
  }

  url = '/finance-esg/financial-control/';

  readonly financialControlPageHeadingTextLocator: Locator = this.page.getByRole('heading', { name: 'Financial Control', exact: true });
}
