import { BasePage } from '@_src/pages/base.page';
import { FrameLocator, Locator, Page } from '@playwright/test';

export class ContactUsPage extends BasePage {
  constructor(page: Page) {
    super(page);
  }

  url = '/contact/';

  readonly contactUsPageHeadingTextLocator: Locator = this.page.getByRole('heading', { name: 'Get in touch' });

  private errorMessageLocator: Locator = this.page.locator('ul[class*="hs-error-msgs"]');

  private contactUsFormLocator: FrameLocator = this.page.frameLocator('iframe[title="Form 0"]');
  private submitButtonLocator: Locator = this.contactUsFormLocator.getByRole('button', { name: 'Submit' });

  private fistNameContainerLocator: Locator = this.contactUsFormLocator.locator('div[class*="hs-firstname"]');
  readonly firstNameFieldValidationMessageLocator: Locator = this.fistNameContainerLocator.locator('label[class*="hs-error-msg"]');

  private lastNameContainerLocator: Locator = this.contactUsFormLocator.locator('div[class*="hs-lastname"]');
  readonly lastNameFieldValidationMessageLocator: Locator = this.lastNameContainerLocator.locator('label[class*="hs-error-msg"]');

  private emailContainerLocator: Locator = this.contactUsFormLocator.locator('div[class*="hs-email"]');
  readonly emailFieldValidationMessageLocator: Locator = this.emailContainerLocator.locator(this.errorMessageLocator);

  private countryContainerLocator: Locator = this.contactUsFormLocator.locator('div[class*="hs-country__new_"]');
  readonly countryFieldValidationMessageLocator: Locator = this.countryContainerLocator.locator(this.errorMessageLocator);

  private howWeHelpContainerLocator: Locator = this.contactUsFormLocator.locator('div[class*="hs-how_can_we_help_you_"]');
  readonly howWeHelpFieldValidationMessageLocator: Locator = this.howWeHelpContainerLocator.locator(this.errorMessageLocator);

  private legalConsentContainerLocator: Locator = this.contactUsFormLocator.locator('div[class*="hs-LEGAL_CONSENT.processing"]');
  readonly legalConsentCheckboxValidationMessageLocator: Locator = this.legalConsentContainerLocator.locator(this.errorMessageLocator);

  private formErrorsContainerLocator: Locator = this.contactUsFormLocator.locator('.hs_error_rollup');
  readonly formErrorsValidationMessageLocator: Locator = this.formErrorsContainerLocator.locator(this.errorMessageLocator);

  async clickSubmitForm(): Promise<void> {
    await this.submitButtonLocator.click();
  }
}
