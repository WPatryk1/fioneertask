# SAP Fioneer webpage tests

Code repository can is available here -> `https://gitlab.com/WPatryk1/fioneertask`

### Local recommended tools:

- VS Code
- Git
- Node.js (version >16)

### Installation and setup

- (optional) install VSC recommended plugins
- install all dependencies: `npm install`
- setup Playwright browsers: `npx playwright install`
- prepare local env file: `cp .env-template .env`
- copy application main URL as `BASE_URL` variable in local `.env` file

## Use

Run all tests:

```
npx playwright test
```

Run all tests with tags:

```
npx playwright test --grep "@TASk"
```

Show report after tests:

```
npm run show-report
```

For more usage cases please look in `package.json` scripts section.
