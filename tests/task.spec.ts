import { TopMenu } from '@_src/components/topMenu.component';
import { HomePage } from '@_src/pages/home.page';
import { expect, test } from '@playwright/test';

test.describe('SAP Fioneer ESG - QA Challenge', () => {
  let topMenu: TopMenu;
  let expectedPageTitle: string;

  test.beforeEach(async ({ page }) => {
    //arrange
    expectedPageTitle = 'SAP Fioneer | World-class software solutions for financial servicesa';
    const homePage = new HomePage(page);
    topMenu = new TopMenu(page);

    //act
    await homePage.goto();

    //assert
    expect(await homePage.title()).toBe(expectedPageTitle);
  });

  test('Should verify all top menu bookmarks are visible @TEST-1 @TASK', async () => {
    // Assert
    await expect(topMenu.bankingBookmarkLocator).toBeVisible();
    await expect(topMenu.insuranceBookmarkLocator).toBeVisible();
    await expect(topMenu.financeEsgBookmarkLocator).toBeVisible();
    await expect(topMenu.servicesBookmarkLocator).toBeVisible();
    await expect(topMenu.partnersBookmarkLocator).toBeVisible();
    await expect(topMenu.companyBookmarkLocator).toBeVisible();
    await expect(topMenu.resourcesBookmarkLocator).toBeVisible();
  });

  test('Should go to Financial Control page @TEST-2 @TASK', async () => {
    //Arrange
    expectedPageTitle = 'SAP Fioneer | Financial Control | Optimise accounting & finance';

    // Act
    const financialControlPage = await topMenu.clickFinancialControlLink();

    // Assert
    expect(await financialControlPage.title()).toBe(expectedPageTitle);
    await expect(financialControlPage.financialControlPageHeadingTextLocator).toBeVisible();
  });

  test('Should verify validation messages after submitting empty contact form @TEST-3 @TASK', async () => {
    const contactUsPage = await test.step('Should go to contact page', async () => {
      //arrange
      expectedPageTitle = 'SAP Fioneer | Contact | Get in touch!';

      //act
      const contactUsPage = await topMenu.clickGetInTouchButton();

      //assert
      expect(await contactUsPage.title()).toBe(expectedPageTitle);
      await expect(contactUsPage.contactUsPageHeadingTextLocator).toBeVisible();

      return contactUsPage;
    });

    await test.step('Should submit empty form verify validation messages for required fields', async () => {
      //arrange
      const emptyRequiredFieldValidationExpectedMessage: string = 'Please complete this required field.';
      const emptyDropdownMenuValidationExpectedMessage: string = 'Please select an option from the dropdown menu.';
      const wholeFormFieldsValidationExpectedMessage: string = 'Please complete all required fields.';

      //act
      await contactUsPage.clickSubmitForm();

      //assert
      await expect(contactUsPage.firstNameFieldValidationMessageLocator).toHaveText(emptyRequiredFieldValidationExpectedMessage);
      await expect(contactUsPage.lastNameFieldValidationMessageLocator).toHaveText(emptyRequiredFieldValidationExpectedMessage);
      await expect(contactUsPage.emailFieldValidationMessageLocator).toHaveText(emptyRequiredFieldValidationExpectedMessage);
      await expect(contactUsPage.countryFieldValidationMessageLocator).toHaveText(emptyDropdownMenuValidationExpectedMessage);
      await expect(contactUsPage.howWeHelpFieldValidationMessageLocator).toHaveText(emptyRequiredFieldValidationExpectedMessage);
      await expect(contactUsPage.legalConsentCheckboxValidationMessageLocator).toHaveText(emptyRequiredFieldValidationExpectedMessage);
      await expect(contactUsPage.formErrorsValidationMessageLocator).toHaveText(wholeFormFieldsValidationExpectedMessage);
    });
  });
});
